﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class SailingState : State<GameState>
{
    private UnityAction unityAction;
    private bool parented;
    private float distance;
    private float timeStamp = 5;
    private bool checkCompass = true;

    public SailingState(GameState owner) : base(owner)
    {
    }

    // Start is called before the first frame update
    public override void Enter()
    {
        base.Enter();
        unityAction += ParentCamera;
        //Toggle camera to go to the sailing scene.
        Owner.NarativeManager.ToggleCamera();
        //filter removal
        GameManager.Instance.StartCoroutine(Extensions.FadeImage(Owner.Filter, new Color(0, 0, 0, 0), 5));
        if (Owner.ObjectiveManager == null)
            Owner.ObjectiveManager = GameManager.Instance.GetManager<ObjectiveManager>();
        GameManager.Instance.StartCoroutine(Extensions.Wait(6f, unityAction));
        GameManager.Instance.GetManager<InputManager>().Boat = Owner.Boat;
    }

    public override void Execute()
    {
        base.Execute();

#if UNITY_EDITOR
        if(Input.GetKeyUp(KeyCode.F))
            Owner.NarativeManager.Conversation();
#endif

        if (Owner.Compass.CompassDirection == CompassDirection.South && checkCompass)
        {

            if (timeStamp > 0)
                timeStamp = Extensions.Timer(timeStamp);
            else
            {
                Debug.Log("Completed");
                Owner.ObjectiveManager.Objectives[Owner.ObjectiveManager.Index].CompletedObjective = true;
                Owner.ObjectiveManager.Objectives[Owner.ObjectiveManager.Index].ExecuteEvent();
                checkCompass = false;
            }
        }
        
    }

    public override void Exit()
    {
        base.Exit();
    }

    public void ParentCamera()
    {
        Vector3 position = Camera.main.transform.position;
        Camera.main.transform.parent = Owner.Boat.transform;
        Camera.main.transform.position = position;
        Camera.main.GetComponent<Animator>().enabled = false;
        Owner.InputManager.InputEnabled = true;
    }
}
