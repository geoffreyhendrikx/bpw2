﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State<T>
{
    protected T Owner
    {
        get;
        set;
    }

    public State(T owner)
    {
        Owner = owner;
    }

    public virtual void Enter() { }
    public virtual void Execute() { }
    public virtual void DelayedExecute() { }
    public virtual void FixedExecute() { }
    public virtual void Exit() { }
}
