﻿using System;
using UnityEngine;

public class StateMachine<T>
{
    public State<T> PreviousState
    {
        get;
        private set;
    }
    public State<T> CurrentState
    {
        get;
        private set;
    }

    /// <summary>
    /// Switches the state.
    /// </summary>
    /// <param name="nextState">Next state</param>
    public void ChangeState(State<T> nextState)
    {
        if (CurrentState == nextState)
            Debug.LogWarning("The current state is the same as the next state.");
        else
            ForceChangeState(nextState);
    }

    /// <summary>
    /// Force switches the state.
    /// </summary>
    /// <param name="nextState">Next state</param>
    public void ForceChangeState(State<T> nextState)
    {
        if (CurrentState != null)
        {
            CurrentState.Exit();
            PreviousState = CurrentState;
        }
        CurrentState = nextState;
        CurrentState.Enter();
    }

    /// <summary>
    /// Reverts the previous state to the current state and the current state to the previous state.
    /// </summary>
    /// <returns>Error code</returns>
    public void RevertToPreviousState()
    {
        if (PreviousState == null)
        {
            Debug.LogWarning("The previous state is null.");
            return;
        }

        // First let's save the previous state before removing it.
        State<T> tempState = PreviousState;
        Debug.Log("Check PreviousState");
        // Then exit the current state and make it our previous state.
        PreviousState = CurrentState;
        CurrentState.Exit();

        // Last but not least, make the temporary state our current state and let it begin.
        CurrentState = tempState;
        CurrentState.Enter();
    }

    /// <summary>
    /// Executes the statemachine.
    /// </summary>
    public void Execute()
    {
        if (CurrentState != null)
            CurrentState.Execute();
    }

    /// <summary>
    /// Executes the statemachine with a delay.
    /// </summary>
    public void DelayedExecute()
    {
        if (CurrentState != null)
            CurrentState.DelayedExecute();
    }

    /// <summary>
    /// Fix executes the statemachine.
    /// </summary>
    public void FixedExecute()
    {
        if (CurrentState != null)
            CurrentState.FixedExecute();
    }
}
