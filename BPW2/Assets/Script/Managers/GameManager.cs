﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{

    private Manager[] managers;

    [Header("NarativeManager")]
    [SerializeField]
    private Animator cameraAnimator;
    [SerializeField]
    private Animator narrativeAnimator;
    [SerializeField]
    private GameObject[] borders;
    [SerializeField]
    private Text storyText;
    [SerializeField]
    private TextAsset story;
    [SerializeField]
    private Text introText;
    [SerializeField]
    private Text conversationText;

    [Space(10)]
    [Header("AudioManager")]
    [SerializeField]
    private AudioSource[] speakers;

    [Space(10)]
    [Header("MissionManager")]
    [SerializeField]
    private Text missionInformation;
    [SerializeField]
    private Objective[] objectives;

    // Start is called before the first frame update
    private void Start()
    { 
        managers = new Manager[5] 
        {
            new InputManager(),
            new GuiManager(),
            new NarativeManager(borders,cameraAnimator,narrativeAnimator,storyText,story,introText, conversationText),
            new AudioManager(speakers[0],speakers[1]),
            new ObjectiveManager(missionInformation,objectives)
        };
        
        for (int i = 0; i < managers.Length; i++)
            managers[i].Start();
    }

    // Update is called once per frame
    private void Update()
    {
        for (int i = 0; i < managers.Length; i++)
            managers[i].Update();
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < managers.Length; i++)
            managers[i].FixedUpdate();
    }

    public int GetSceneIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public void SwitchScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    /// <summary>
    /// Get the certain manager.
    /// </summary>
    /// <typeparam name="T">Certain manager</typeparam>
    /// <returns>T</returns>
    public T GetManager<T>() where T : Manager
    {
        //looped through each managers
        for (int i = 0; i < managers.Length; i++)
            if (managers[i].GetType() == typeof(T))
                return (T)managers[i];

        return default(T);
    }
}
