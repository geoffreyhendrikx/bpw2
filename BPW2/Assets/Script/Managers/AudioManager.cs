﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Manager
{
    private AudioSource audioSpeaker;
    private AudioSource backgroundSpeaker;

    public AudioManager(AudioSource audioSpeaker,AudioSource backgroundSpeaker)
    {
        this.audioSpeaker = audioSpeaker;
        this.backgroundSpeaker = backgroundSpeaker;
    }

    public void StartIntroMusic()
    {

    }
}
