﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class NarativeManager : Manager
{
    private TextAsset story;
    private AudioManager audioManager;
    private InputManager inputManager;
    private GameObject[] borders;
    private Animator cameraAnim;
    private Animator narativeAnim;

    private Text storyText;
    private Text introText;
    private Text conversationText;

    private bool isInCutscene;
    private bool inCutscene;
    public bool InCutscene
    {
        get
        {
            return inCutscene;
        }
        set
        {
            if (value)
                inputManager.InputEnabled = false;
            else
                inputManager.InputEnabled = true;

            inCutscene = value;

        }
    }
    private int currentLine = -1;

    private string[] lines;

    private bool inNarrative;
    public bool InNarrative
    {
        get
        {
            return inNarrative;
        }
        set
        {
            inNarrative = value;
        }
    }

    private float borderAnimationTime = 1.3f;

    public NarativeManager(GameObject[] borders, Animator cameraAnim,Animator narativeAnim, Text storyText, TextAsset story, Text introText, Text conversationText)
    {
        this.borders = borders;
        this.cameraAnim = cameraAnim;
        this.narativeAnim = narativeAnim;
        this.storyText = storyText;
        this.story = story;
        this.introText = introText;
        this.conversationText = conversationText;
    }

    public void ToggleNarative()
    {
        inNarrative = !inNarrative;

        if (inNarrative)
            narativeAnim.SetTrigger("BeginCutScene");
        else
            narativeAnim.SetTrigger("EndCutScene");

    }

    // Start is called before the first frame update
    public override void Start()
    {
        inputManager = GameManager.Instance.GetManager<InputManager>();
        audioManager = GameManager.Instance.GetManager<AudioManager>();

        lines = story.ToString().Split('.');
    }

    public bool NextLine()
    {
        currentLine++;
        if (currentLine == 3)
        {
            StartIntro();
            return false;
        }
        else
        {
            storyText.text = lines[currentLine];
            return true;
        }
        
    }

    public void ToggleCamera()
    {
        cameraAnim.SetTrigger("MoveCamera");
    }
    /// <summary>
    /// Start the begin intro.
    /// </summary>
    public void StartIntro()
    {
        Color toColor = new Color(introText.color.r, introText.color.g, introText.color.b, 1);
        GameManager.Instance.StartCoroutine(Extensions.FadeTextColor(introText,introText.color, toColor ,3, true,2));
        GameManager.Instance.GetManager<AudioManager>().StartIntroMusic();
        GameManager.Instance.GetManager<ObjectiveManager>().GetNextObjective();
    }

    /// <summary>
    /// Shows conversation of the crew.
    /// </summary>
    public void Conversation()
    {
        conversationText.text = lines[currentLine].ToString();
    }
}
