﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CinematicScreen : MonoBehaviour
{
    [SerializeField]
    private Image blackScreen;
    private Color alphaColor = new Color(0, 0, 0, 0);
    private Color blackColor = new Color(0, 0, 0, 1);
    [SerializeField]
    private int crossFadeTime;
    private FadeBlackScreen fadeScreen = FadeBlackScreen.FadeOut;

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(WaitFewMiliSecond());

        ToggleBlackScreen();
    }

    private IEnumerator WaitFewMiliSecond()
    {
        yield return new WaitForSeconds(.1f);
        if (GameManager.Instance.GetSceneIndex() == 0)
            GameState.Instance.StateMachine.ChangeState(GameState.Instance.AvailableStates[(int)States.CinematicState]);
    }

    public void ToggleBlackScreen()
    {
        if (fadeScreen == FadeBlackScreen.FadeOut)
        {
            blackScreen.CrossFadeAlpha(0, crossFadeTime, false);
            fadeScreen = FadeBlackScreen.FadeIn;
        }
        else
        {
            blackScreen.CrossFadeAlpha(0, crossFadeTime, false);
            fadeScreen = FadeBlackScreen.FadeOut;
        }
    }

}

enum FadeBlackScreen
{
    FadeIn,
    FadeOut
}
