﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{
    [SerializeField]
    private Rigidbody myRb;
    public Rigidbody MyRb
    {
        get
        {
            return myRb;
        }
        set
        {
            myRb = value;
        }
    }

    // Start is called before the first frame update
    private void Start() { }

    // Update is called once per frame
    private void Update() {  }
}
