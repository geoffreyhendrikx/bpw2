﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Manager
{
    private bool inputEnabled;
    public bool InputEnabled
    {
        get
        {
            return inputEnabled;
        }
        set
        {
            inputEnabled = value;
        }
    }

    //Getting the player
    private BoatController boat;
    public BoatController Boat
    {
        get
        {
            return boat;
        }
        set
        {
            boat = value;
        }
    }

    private float vertical
    {
        get
        {
            return Input.GetAxis("Vertical");
        }
    }
    private float horizontal
    {
        get
        {
            return Input.GetAxis("Horizontal");
        }
        set
        {
            horizontal = value;
        }
    }
    
    private LerpState lerpingState = LerpState.nothing;

    private int sceneNumber;

    // Start is called before the first frame update
    public override void Start()
    {
        sceneNumber = GameManager.Instance.GetSceneIndex();

    }
    

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (sceneNumber != 1)
        {
            if (inputEnabled)
            {
                boat.MoveRigidbody(vertical);
                boat.RotateBoat(horizontal);
            }
        }
        else
        {
            if (!boat)
                boat = FindObjectOfType<BoatController>();

            boat.MoveRigidbody(100);
            boat.RotateBoat(horizontal);
            Debug.Log("Try");
        }
    }

    public bool NarativeStory()
    {
        if (Input.GetKey(KeyCode.Space))
            return true;
        return false;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if(inputEnabled)
        {

        }
    }

}
public enum LerpState
{
    forwards,
    backwards,
    nothing
}