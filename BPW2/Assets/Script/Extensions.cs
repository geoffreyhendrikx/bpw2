﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public static class Extensions
{

    /// <summary>
    /// Fading the color into another color
    /// </summary>
    public static IEnumerator FadeTextColor(Text storyText, Color currentColor, Color endColor, float overTime, bool reverse = false, float reverseTime = 0)
    {
        float startTime = Time.time;
        Color startColor = currentColor;

        while (Time.time < (startTime + overTime))
        {
            storyText.color = Color.Lerp(currentColor, endColor, (Time.time - startTime) / overTime);
            yield return null;
        }

        if (reverse)
            GameManager.Instance.StartCoroutine(FadeTextColor(storyText, endColor, new Color(currentColor.r, currentColor.g, currentColor.b, 0), 2, true));
    }

    public static IEnumerator FadeImage(Image image, Color endColor, float overTime)
    {
        float startTime = Time.time;
        Color currentColor = image.color;

        while (Time.time < (startTime + overTime))
        {
            image.color = Color.Lerp(currentColor, endColor, (Time.time - startTime) / overTime);
            yield return null;
        }
    }

    /// <summary>
    /// Spherical linear interpolation between 2 objects
    /// </summary>
    /// <param name="transformObject">This transform</param>
    /// <param name="targetTransform">Target</param>
    /// <param name="timeStamp">Time stamp for the lerp</param>
    public static IEnumerator SlerpingObjects(Transform transformObject, Transform targetRotation, float overTime, UnityAction callback)
    {
        float startTime = Time.time;
        Vector3 currentPosition = transformObject.position;

        while (Time.time < (startTime + overTime))
        {
            transformObject.rotation = Quaternion.Slerp(transformObject.rotation, targetRotation.rotation, (Time.time - startTime) / overTime);
            yield return null;
        }

        callback?.Invoke();
    }

    /// <summary>
    /// liniare interpolation between 2 objects
    /// </summary>
    /// <param name="transformObject">This transform</param>
    /// <param name="targetTransform">Target</param>
    /// <param name="timeStamp">Time stamp for the lerp</param>
    public static IEnumerator LerpingObjects(Transform transformObject, float targetPosition, float overTime)
    {
        float startTime = Time.time;
        Vector3 currentPosition = transformObject.position;
        Vector3 target = new Vector3(currentPosition.x + targetPosition, currentPosition.y, currentPosition.z);


        while (Time.time < (startTime + overTime))
        {
            transformObject.position = Vector3.Lerp(currentPosition, target, (Time.time - startTime) / overTime);
            yield return null;
        }
    }


    public static IEnumerator Wait(float time, UnityAction unityAction)
    {
        yield return new WaitForSeconds(time);
        unityAction.Invoke();
    }

    public static float Timer(float time)
    {
        return (time -= Time.deltaTime);
    }


}
