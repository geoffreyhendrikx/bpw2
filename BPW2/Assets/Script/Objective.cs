﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Objective : MonoBehaviour
{
    //index of the objectives.
    private int index;
    public int Index
    {
        get
        {
            return index;
        }
        set
        {
            index = value;
        }
    }

    /// <summary>
    /// Header of the Objective information
    /// </summary>
    [SerializeField]
    private string header;
    public string Header
    {
        get
        {
            return header;
        }
        set
        {
            header = value;
        }
    }

    /// <summary>
    /// Mission Information
    /// </summary>
    [SerializeField]
    private string missionInformation;
    public string MissionInformation
    {
        get
        {
            return missionInformation;
        }
        set
        {
            missionInformation = value;
        }
    }

    /// <summary>
    /// Completed objective
    /// </summary>
    [SerializeField]
    private bool completedObjective;
    public bool CompletedObjective
    {
        get
        {
            return completedObjective;
        }
        set
        {
            completedObjective = value;
        }
    }

    [SerializeField]
    private CompassDirection direction;
    public CompassDirection Direction
    {
        get
        {
            return direction;
        }
        private set
        {
            direction = value;
        }
    }

    [SerializeField]
    private UnityEvent unityEvent;

    private void Start()
    {
        GameManager.Instance.StartCoroutine(WaitFewMiliSecond());
    }

    private IEnumerator WaitFewMiliSecond()
    {
        yield return new WaitForSeconds(2f);
    }

    public void ExecuteEvent()
    {
        StartCoroutine(WaitForExecuteEvent());   
    }

    private IEnumerator WaitForExecuteEvent()
    {
        yield return new WaitForSeconds(3);
        unityEvent.Invoke();
    }
    
    
}
