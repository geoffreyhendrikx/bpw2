﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : Singleton<GameState>
{

    private InputManager inputManager;
    public InputManager InputManager
    {
        get
        {
            return GameManager.Instance.GetManager<InputManager>();
        }
        set
        {
            inputManager = value;
        }
    }

    private NarativeManager narativeManager;
    public NarativeManager NarativeManager
    {
        get
        {
            return GameManager.Instance.GetManager<NarativeManager>();
        }
        set
        {
            narativeManager = value;
        }
    }

    //Statemachine Instance
    private StateMachine<GameState> stateMachine = new StateMachine<GameState>();
    public StateMachine<GameState> StateMachine
    {
        get
        {
            return stateMachine;
        }
        set
        {
            stateMachine = value;
        }
    }

    [SerializeField]
    private Image filter;
    public Image Filter
    {
        get
        {
            return filter;
        }
        set
        {
            filter = value;
        }
    }

    //AvailableStates
    private State<GameState>[] availableStates;
    public State<GameState>[] AvailableStates
    {
        get
        {
            return availableStates;
        }
        set
        {
            availableStates = value;
        }
    }

    [SerializeField]
    private BoatController boat;
    public BoatController Boat
    {
        get
        {
            return boat;
        }
        set
        {
            boat = value;
        }
    }

    private ObjectiveManager objectiveManager;
    public ObjectiveManager ObjectiveManager
    {
        get
        {
            return objectiveManager;
        }
        set
        {
            objectiveManager = value;
        }
    }

    [SerializeField]
    private Compass compass;
    public Compass Compass
    {
        get
        {
            return compass;
        }
        set
        {
            compass = value;
        }

    }

    private void Start()
    {
        availableStates = new State<GameState>[2]
        {
            new CinematicState(this),
            new SailingState(this)
        };
    }

    // Update is called once per frame
    private void Update()
    {
        stateMachine.Execute();
    }

    private void LateUpdate()
    {
        stateMachine.DelayedExecute();
    }

    private void FixedUpdate()
    {
        stateMachine.FixedExecute();
    }
}
public enum States
{
    CinematicState,
    SailingState
}