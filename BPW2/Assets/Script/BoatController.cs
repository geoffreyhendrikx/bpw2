﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatController : Actor
{
    [Header("Movement options")]
    [SerializeField]
    [Range(0, 5)]
    private float speed;
    [SerializeField]
    [Range(0, 15)]
    private float maxSpeed;

    [Header("Rotation")]
    [SerializeField]
    private GameObject steerDirection;
    [SerializeField]
    private float rotationSpeed;

    /// <summary>
    /// Moving the rigidbody with force in the x and z axis.
    /// </summary>
    /// <param name="rb">Rigidbody of the object</param>
    /// <param name="inputX">X Input</param>
    /// <param name="inputZ">Z Input</param>
    /// <param name="speed">Speed of the Object</param>
    public void MoveRigidbody(float inputZ)
    {
        Vector3 Movement;
        if (GameManager.Instance.GetSceneIndex() != 1)
            Movement = (MyRb.gameObject.transform.forward * (inputZ * (speed * 2)));
        else
        {
            MyRb.velocity = Vector3.zero;
            Movement = (transform.forward + MyRb.gameObject.transform.forward * (inputZ * (speed * 100)));
        }
        //check the speed of  the boat
        if (MyRb.velocity.magnitude < maxSpeed)
            MyRb.AddForce(Movement);

    }

    /// <summary>
    /// Rotate the boat.
    /// </summary>
    public void RotateBoat(float inputX)
    {
        Debug.Log("Rotate");
        MyRb.transform.eulerAngles = new Vector3(MyRb.transform.eulerAngles.x, MyRb.transform.eulerAngles.y + (inputX * rotationSpeed), MyRb.transform.eulerAngles.z);
    }
}
