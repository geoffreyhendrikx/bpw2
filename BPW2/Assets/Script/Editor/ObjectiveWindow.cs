﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// This window is just for basic information which objectives the game has.
/// I know sicke editor scripting skills @valetijn
/// </summary>
public class ObjectiveWindow : EditorWindow
{

    private Objective[] objectives
    {
        get
        {
            return FindObjectsOfType<Objective>();
        }
        set
        {
            objectives = value;
        }
    }

    private GUIStyle font;
    private GUIStyle style;
    private Vector2 currentScrollPosition;

    [MenuItem("Window/Objectives Information")]
    private static void Init()
    {
        ObjectiveWindow window = (ObjectiveWindow)EditorWindow.GetWindow(typeof(ObjectiveWindow));
        GUIContent titleContent = new GUIContent("Objectives");
        window.titleContent = titleContent;
        Texture2D icon = Resources.Load<Texture2D>("EditorIcons/Note");
        titleContent.image = icon;
        window.Show();
    }

    /// <summary>
    /// Draws on screen
    /// </summary>
    public void OnGUI()
    {
        //Make the gui styles
        if (font == null)
        {
            font = new GUIStyle();
            font.fontSize = 20;
        }
        if (style == null)
        {
            style = new GUIStyle();
            style.alignment = TextAnchor.MiddleCenter;
            style.fontStyle = FontStyle.Bold;
            style.fontSize = 20;
        }

        //explain what the editor window is
        EditorGUILayout.BeginScrollView(currentScrollPosition);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Note of all Missions", style);
        EditorGUILayout.EndHorizontal();

        if (objectives.Length > 0)
        {
            //setting all the objectives in a row
            for (int i = 0; i < objectives.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField((i + 1).ToString() + ". " + objectives[i].Header, font);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("\t" + objectives[i].MissionInformation);
                EditorGUILayout.EndHorizontal();
            }
        }

        if (GUILayout.Button("Create Objective"))
        {
            GameObject go = new GameObject();
            go.name = "Objective " + (objectives.Length + 1);
            go.AddComponent<Objective>();
        }

        EditorGUILayout.EndScrollView();


    }
}
